﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StackController : MonoBehaviour
{
    private const float TILE_SIZE = 8f;
    private const float TILE_HEIGHT = 1f;
    private const float TILE_MOVEMENT_OFFSET = 1f;
    private const float TILE_SPEED = 2.5f;
    private const float STACK_SPEED = 0.5f;
    private const int STACK_MOVING_THRESHOLD = 4;
    private const float OK_THRESHOLD = 0.1f;
    private const int COMBO_THRESHOLD = 5;
    private const float COMBO_GAIN = 0.25f;
    private const string STORE_BEST = "BEST";

    public GameObject CosmeticsController;
    public Text scoreText;
    public Text bestScoreText;

    public GameObject start;
    public GameObject scoreInfo;
    public GameObject end;

    private Color color;
    private List<GameObject> stack = new List<GameObject>();
    private Vector3 bounds = new Vector3(TILE_SIZE, TILE_HEIGHT, TILE_SIZE);

    private bool gameOver = false;
    private bool gameStarted = false;
    private int score = 0;
    private int combo = 0;
    private int index;

    private float transition = 0.0f;
    private bool xAxis = true;

    private Vector3 stackTargetPosition = new Vector3(0, 0, 0);
    private Vector3 previousTilePosition;

    // Use this for initialization
    void Start()
    {
        gameOver = false;
        gameStarted = false;

        start.SetActive(true);
        end.SetActive(false);
        scoreInfo.SetActive(false);

        bestScoreText.text = PlayerPrefs.GetInt(STORE_BEST, 0).ToString();
        for (int i = 0; i < transform.childCount; i++)
        {
            color = CosmeticsController.GetComponent<CosmeticsController>().GetColor(i * Mathf.PI);
            transform.GetChild(i).gameObject.GetComponent<MeshRenderer>().material.color = color;
            stack.Add(transform.GetChild(i).gameObject);
        }

        index = (transform.childCount - 1);
        Debug.Log(index);
    }
	
    // Update is called once per frame
    private void Update()
    {
        if (!gameOver && gameStarted && Input.GetMouseButtonDown(0))
        {
            if (Drop())
            {
                color = CosmeticsController.GetComponent<CosmeticsController>().GetColor(score);
                Spawn();
                score++;
                scoreText.text = score.ToString();
            }
            else
            {
                GameOver();
            }
        }
        else if (!gameOver && !gameStarted && Input.GetMouseButtonDown(0))
        {
            gameStarted = true;
            gameOver = false;
            start.SetActive(false);
            scoreInfo.SetActive(true);
        }
        else if (Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene("stack_game");
        }

        if (gameStarted && !gameOver)
        {
            Move();

            // Move stack down
            transform.position = Vector3.Lerp(transform.position, stackTargetPosition, Time.deltaTime);
        }
    }

    private void CreateFallingPiece(float error)
    {
        GameObject fallingPiece = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Vector3 position = stack[index].transform.position;

        if (xAxis)
        {
            fallingPiece.transform.localScale = new Vector3(
                error, 
                TILE_HEIGHT, 
                bounds.z
            );
            fallingPiece.transform.localPosition = new Vector3(
                (position.x > 0) ? position.x + (bounds.x / 2) : position.x - (bounds.x / 2), 
                position.y, 
                position.z
            );
        }
        else
        {
            fallingPiece.transform.localScale = new Vector3(
                bounds.x, 
                TILE_HEIGHT, 
                error
            );
            fallingPiece.transform.localPosition = new Vector3(
                position.x, 
                position.y, 
                (position.z > 0) ? position.z + (bounds.z / 2) : position.z - (bounds.z / 2)
            );
        }

        fallingPiece.GetComponent<MeshRenderer>().material.color = color;
        fallingPiece.AddComponent<Rigidbody>();
    }

    private bool Drop()
    {
        bool ok = true;
        float error = 0f;

        if (xAxis)
        {
            error = previousTilePosition.x - stack[index].transform.position.x;
            if (Mathf.Abs(error) > OK_THRESHOLD)
            {
                combo = 0;
                bounds.x -= Mathf.Abs(error);

                if (bounds.x <= 0)
                {
                    ok = false;
                }
                else
                {
                    stack[index].transform.localScale = bounds;
                    CreateFallingPiece(error);
                    stack[index].transform.localPosition = new Vector3(
                        stack[index].transform.localPosition.x + (error / 2), 
                        score, 
                        previousTilePosition.z
                    );
                }
            }
            else if (Mathf.Abs(error) <= OK_THRESHOLD)
            {
                ++combo;
                PlaceOnPrevious();
            }
        }
        else
        {
            error = previousTilePosition.z - stack[index].transform.position.z;
            if (Mathf.Abs(error) > OK_THRESHOLD)
            {
                combo = 0;
                bounds.z -= Mathf.Abs(error);

                if (bounds.z <= 0)
                {
                    ok = false;
                }
                else
                {
                    CreateFallingPiece(error);
                    stack[index].transform.localScale = bounds;
                    stack[index].transform.localPosition = 
                        new Vector3(
                        previousTilePosition.x, 
                        score, 
                        stack[index].transform.localPosition.z + (error / 2)
                    ); 
                }
            }
            else if (Mathf.Abs(error) <= OK_THRESHOLD)
            {
                ++combo;
                PlaceOnPrevious();
            }
        }

        xAxis = !xAxis;

        if (ok && score > STACK_MOVING_THRESHOLD)
        {
            stackTargetPosition.y -= 1;
        }

        return ok; 
    }

    private void Spawn()
    {
        previousTilePosition = stack[index].transform.localPosition;

        GameObject tile = GameObject.CreatePrimitive(PrimitiveType.Cube);
        tile.transform.localPosition = new Vector3(0, score - 1, 0);
        tile.transform.localScale = bounds;
        tile.transform.SetParent(this.transform);
        tile.GetComponent<MeshRenderer>().material.color = color;
        stack.Add(tile);
        index++;
    }

    private void GameOver()
    {
        // TODO efecte camera

        end.SetActive(true);

        stack[index].AddComponent<Rigidbody>();
        gameOver = true;

        if (score > PlayerPrefs.GetInt(STORE_BEST, 0))
        {
            PlayerPrefs.SetInt(STORE_BEST, score);
        }
    }

    private void Move()
    {

        transition += Time.deltaTime * TILE_SPEED;
        float oscillation = (TILE_SIZE + TILE_MOVEMENT_OFFSET) * Mathf.Sin(transition);


        stack[index].transform.localPosition = 
            (xAxis) ? 
            new Vector3(oscillation, score, previousTilePosition.z) : 
            new Vector3(previousTilePosition.x, score, oscillation);
    }

    private void PlaceOnPrevious()
    {
        stack[index].transform.localPosition = 
            new Vector3(
            previousTilePosition.x, 
            score, 
            previousTilePosition.z
        );
    }
}
