﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosmeticsController : MonoBehaviour
{
    private float COLOR_SMOOTHER = 0.005f;

    private Color baseColor;
    private Color targetColor;
    private Color currentColor;

    private float lerpingInterval = 0f;

    public GameObject background;

    // Use this for initialization
    void Start()
    {

        baseColor = GenerateRandomColor();
        background.GetComponent<MeshRenderer>().material.SetColor("_BottomColor", baseColor);
        currentColor = baseColor;
        targetColor = GenerateRandomColor();
    }
	
    // Update is called once per frame
    void Update()
    {
		
    }

    public Color GetColor(float interval)
    {

        lerpingInterval = (lerpingInterval > 1f) ? 0.5f : lerpingInterval + Mathf.Abs(Mathf.Sin(interval * COLOR_SMOOTHER));


        currentColor = Color.Lerp(currentColor, targetColor, lerpingInterval);
        background.GetComponent<MeshRenderer>().material.SetColor("_TopColor", currentColor);

        if (currentColor.Equals(targetColor))
        {
            targetColor = GenerateRandomColor();
        }

        return currentColor;
    }

    private Color GenerateRandomColor()
    {
        Color color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));

        return color;
    }
}
